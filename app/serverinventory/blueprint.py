from flask import Blueprint
from flask import render_template
from models import ServersInventory,ServerPhyLocation,ServerDatabaseType,ServerOsType,ServerOsVersion
from flask import request
from .forms import ServerForm
from app import db
from flask import redirect,url_for
from flask_security import login_required,roles_required,roles_accepted

serinv = Blueprint('serverinventory',__name__,template_folder='templates')



@serinv.route('/add_ser', methods=['POST','GET'])
def add_server():
    if request.method=='POST':
        ser_hostname = request.form['ser_hostname']
        ser_ip = request.form['ser_ip']
        ser_description = request.form['ser_description']
        ser_cpu = request.form['ser_cpu']
        ser_ram = request.form['ser_ram']
        ser_storage = request.form['ser_storage']
        ser_phy_location = request.form['ser_phy_location']
        ser_os_type = request.form['ser_os_type']
        ser_os_version = request.form['ser_os_version']
        ser_database_type = request.form['ser_database_type']
        try:
            new_server = ServersInventory(ser_hostname = ser_hostname,ser_ip=ser_ip, ser_description=ser_description,ser_cpu=ser_cpu,ser_ram=ser_ram,ser_storage=ser_storage,\
                 ser_phy_location_id=ser_phy_location, ser_os_type_id = ser_os_type, ser_os_version_id = ser_os_version,ser_database_type_id=ser_database_type)
            db.session.add(new_server)
            db.session.commit()
            return redirect(url_for('serverinventory.index'))
        except Exception as e:
            error = "Something is wrong!"
            print(e)
    form = ServerForm()
    return render_template('serverinventory/add_server.html', form = form)


@serinv.route('/')
def index():
    servers = ServersInventory.query.order_by(ServersInventory.id.asc()).join(ServerPhyLocation, ServersInventory.ser_phy_location_id==ServerPhyLocation.id).join(ServerDatabaseType, ServersInventory.ser_database_type_id==ServerDatabaseType.id)\
        .join(ServerOsType, ServersInventory.ser_os_type_id==ServerOsType.id).join(ServerOsVersion, ServersInventory.ser_os_version_id==ServerOsVersion.id)
    
    return render_template('serverinventory/index.html',servers=servers)