from wtforms import Form, StringField, TextAreaField, SelectField
from wtforms_sqlalchemy.fields import QuerySelectField
from models import ServerPhyLocation,ServerDatabaseType,ServerOsType,ServerOsVersion

class ServerForm(Form):
    ser_hostname = StringField('Hostname')
    ser_ip = StringField('IP address')
    ser_os_type = SelectField('OS Type',choices=[(g.id, g.ser_os_type) for g in ServerOsType.query])
    ser_os_version = SelectField('OS Version',choices=[(g.id, g.ser_os_version) for g in ServerOsVersion.query])
    ser_description = StringField('Description')
    ser_cpu = StringField('CPU')
    ser_ram = StringField('RAM')
    ser_storage = StringField('Storage')
    ser_phy_location = SelectField('Physical Location',choices=[(g.id, g.ser_phy_location) for g in ServerPhyLocation.query])
    ser_database_type = SelectField('Database Type',choices=[(g.id, g.ser_database_type) for g in ServerDatabaseType.query])