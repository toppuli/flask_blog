class Configuration(object):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'mysql+mysqlconnector://yusif:My162636@localhost/flask'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = "My Super Secret Key"

    #FLASK SECURITY
    SECURITY_PASSWORD_SALT='azerduz'
    SECURITY_PASSWORD_HASH='sha512_crypt'
    SECURITY_REGISTERABLE=True
    SECURITY_CONFIRMABLE=False