from flask import Blueprint
from flask import render_template
from models import Posts, Tags
from flask import request
from .forms import PostForm
from app import db
from flask import redirect,url_for
from flask_security import login_required,roles_required,roles_accepted


blogposts = Blueprint('blog',__name__,template_folder='templates')

@blogposts.route('/create', methods=['POST','GET'])
@login_required
@roles_accepted('editor','admin')
def create_post():
    if request.method == 'POST':
        t = request.form['title']
        b = request.form['body']
        try:
            new_post = Posts(title = t,body = b)
            db.session.add(new_post)
            db.session.commit()
            return redirect(url_for('blog.index'))
        except:
            error = "Something wrong!"

    form = PostForm()
    return render_template('blog/create_post.html', form = form)

@blogposts.route('/')
def index():
    q = request.args.get('q')
    page = request.args.get('page')
    if page and page.isdigit():
        page = int(page)
    else:
        page = 1
    
    if q:
        posts = Posts.query.filter(Posts.title.contains(q) | Posts.body.contains(Posts.body.contains(q)))
    else:
        posts = Posts.query.order_by(Posts.created.desc())

    pages = posts.paginate(page=page, per_page=3)
    return render_template('blog/index.html',posts = posts,pages = pages)

@blogposts.route('/<slug>')
def post_details(slug):
    post_content = Posts.query.filter(Posts.slug == slug).first()
    tags = post_content.tags
    return render_template('blog/blog_post.html',post=post_content,tags=tags)

@blogposts.route('/tag/<slug>/')
def tag_details(slug):
    
    page = request.args.get('page')
    if page and page.isdigit():
        page = int(page)
    else:
        page = 1

    tag = Tags.query.filter(Tags.slug == slug).first()
    posts = tag.posts
    #posts = Posts.query.order_by(Posts.created.desc())

    pages = posts.paginate(page=page, per_page=3)
    return render_template('blog/tag_post.html', tag = tag, posts = posts,pages = pages)