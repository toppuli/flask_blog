from flask import Flask
from config import Configuration

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate,MigrateCommand
from flask_script import Manager

from flask_admin import Admin, AdminIndexView
from flask_admin.contrib.sqla import ModelView

from flask_security import SQLAlchemyUserDatastore
from flask_security import Security, utils
from flask_security import current_user

from flask import request, url_for, redirect
from wtforms.fields import PasswordField


app = Flask(__name__)
app.config.from_object(Configuration)
db = SQLAlchemy(app)


migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)

#Admin panel
from models import *

class AdminMixin:
    def is_accessible(self):
        return current_user.has_role('admin')

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('security.login',next=request.url))

class BaseModelView(ModelView):
    def on_model_change(self, form, model, is_created):
        model.generate_slug()
        return super(BaseModelView, self).on_model_change(form, model, is_created)

class AdminView(AdminMixin, ModelView):
    pass

class HomeAdminView(AdminMixin, AdminIndexView):
    pass

class PostsAdminView(AdminMixin, BaseModelView):
    form_columns = ['title','body','tags']

class TagsAdminView(AdminMixin, BaseModelView):
    form_columns = ['name','posts']

class UserAdminView(AdminMixin,ModelView):
    column_exclude_list = ('password',)
    form_excluded_columns = ('password',)
    column_auto_select_related = True

    def scaffold_form(self):
        form_class = super(UserAdminView, self).scaffold_form()
        form_class.password2 = PasswordField('New Password')
        return form_class

    def on_model_change(self, form, model, is_created):
        if len(model.password2):
            model.password = utils.encrypt_password(model.password2)

class RoleAdminView(AdminMixin, ModelView):
    pass

class ServerElementsAdminView(ModelView):
    form_excluded_columns = ('id')

class ServersInventoryAdminView(AdminMixin,ServerElementsAdminView):
    pass

class ServerPhyLocationAdminView(AdminMixin,ServerElementsAdminView):
    pass

class ServerDatabaseTypeAdminView(AdminMixin,ServerElementsAdminView):
    pass

class ServerOsTypeAdminView(AdminMixin,ServerElementsAdminView):
    pass

class ServerOsVersionAdminView(AdminMixin,ServerElementsAdminView):
    pass



admin = Admin(app, 'FlaskBlog', url='/', index_view=HomeAdminView(name='Home'))
admin.add_view(PostsAdminView(Posts, db.session, category='Blog'))
admin.add_view(TagsAdminView(Tags, db.session, category='Blog'))

#Security Admin View
admin.add_view(UserAdminView(User,db.session, category='Security'))
admin.add_view(RoleAdminView(Role, db.session, category = 'Security'))

#Server inventory Admin VIEW
admin.add_view(ServersInventoryAdminView(ServersInventory, db.session, category = 'Server Inventory'))
admin.add_view(ServerPhyLocationAdminView(ServerPhyLocation, db.session, category='Server Inventory' ))
admin.add_view(ServerDatabaseTypeAdminView(ServerDatabaseType, db.session, category='Server Inventory'))
admin.add_view(ServerOsTypeAdminView(ServerOsType, db.session, category='Server Inventory'))
admin.add_view(ServerOsVersionAdminView(ServerOsVersion, db.session, category='Server Inventory'))

#Security
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)