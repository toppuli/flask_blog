from app import app
import view
from app import db
from blog.blueprint import blogposts
from serverinventory.blueprint import serinv

app.register_blueprint(blogposts,url_prefix='/blog')
app.register_blueprint(serinv, url_prefix='/serverinventory')
if __name__ == '__main__':
    app.run()