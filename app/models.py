from app import db
from datetime import datetime
import re
from flask_security import UserMixin,RoleMixin

def slugify(s):
    pattern = r'[^\w+]'
    return re.sub(pattern, '-', s).lower()

post_tags = db.Table('post_tags', db.Column('post_id',db.Integer, db.ForeignKey('posts.id')),
                    db.Column('tag_id', db.Integer, db.ForeignKey('tags.id'))                    )

class Posts(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(140))
    slug = db.Column(db.String(140), unique=True)
    body = db.Column(db.Text)
    created = db.Column(db.DateTime, default=datetime.now())

    def __init__(self,*args, **kwargs):
        super(Posts, self).__init__(*args,**kwargs)
        self.generate_slug()

    tags = db.relationship('Tags', secondary=post_tags,backref=db.backref('posts',lazy='dynamic'))

    def generate_slug(self):
        if self.title:
            self.slug = slugify(self.title)


    def __repr__(self):
        return "<Post id: {}, title: {}>".format(self.id,self.title)


class Tags(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(140))
    slug = db.Column(db.String(140), unique=True)

    def __init__(self, *args, ** kwargs):
        super(Tags, self).__init__(*args, **kwargs)
        self.slug = self.generate_slug()
    
    def generate_slug(self):
        self.slug = slugify(self.name)


    def __repr__(self):
        return "{}".format(self.name)


##Flask Security

roles_users = db.Table('roles_users',
        db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
        db.Column('role_id', db.Integer(), db.ForeignKey('role.id'))
    )

class User(db.Model, UserMixin):
    id = db.Column(db.Integer(), primary_key=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    roles = db.relationship('Role', secondary=roles_users, backref = db.backref('users', lazy='dynamic'))

    def __repr__(self):
        return "{}".format(self.email)

class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(100),unique=True)
    description = db.Column(db.String(255))

    def __repr__(self):
        return "{}".format(self.name)

class ServerDatabaseType(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    ser_database_type = db.Column(db.String(50))

    def __init__(self, *args, ** kwargs):
        super(ServerDatabaseType, self).__init__(*args, **kwargs)

    def __repr__(self):
        return "{},{}".format(self.id,self.ser_database_type)

    def __repr__(self):
        return "{}".format(self.ser_database_type)

class ServerPhyLocation(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    ser_phy_location = db.Column(db.String(50))

    def __init__(self, *args, ** kwargs):
        super(ServerPhyLocation, self).__init__(*args, **kwargs)

    def __repr__(self):
        return "{},{}".format(self.id,self.ser_phy_location)

    def __repr__(self):
        return "{}".format(self.ser_phy_location)

class ServerOsType(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    ser_os_type = db.Column(db.String(50))

    def __init__(self, *args, ** kwargs):
        super(ServerOsType, self).__init__(*args, **kwargs)

    def __repr__(self):
        return "{},{}".format(self.id,self.ser_os_type)

    def __repr__(self):
        return "{}".format(self.ser_os_type)

class ServerOsVersion(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    ser_os_version = db.Column(db.String(50))

    def __init__(self, *args, ** kwargs):
        super(ServerOsVersion, self).__init__(*args, **kwargs)

    def __repr__(self):
        return "{},{}".format(self.id,self.ser_os_version)

    def __repr__(self):
        return "{}".format(self.ser_os_version)

class ServersInventory(db.Model):
    id = db.Column(db.Integer(),primary_key=True)
    ser_hostname = db.Column(db.String(100),unique=True)
    ser_ip = db.Column(db.String(50),unique=True)
    ser_description = db.Column(db.String(100))
    ser_cpu = db.Column(db.String(10))
    ser_ram = db.Column(db.String(10))
    ser_storage = db.Column(db.String(10))
    ser_created = db.Column(db.DateTime, default=datetime.now())
    ser_phy_location_id = db.Column(db.Integer(),db.ForeignKey('server_phy_location.id'))
    ser_database_type_id = db.Column(db.Integer(),db.ForeignKey('server_database_type.id'))
    ser_os_type_id = db.Column(db.Integer(),db.ForeignKey('server_os_type.id'))
    ser_os_version_id = db.Column(db.Integer(),db.ForeignKey('server_os_version.id'))
    ser_phy_location = db.relationship('ServerPhyLocation')
    ser_database_type = db.relationship('ServerDatabaseType')
    ser_os_type = db.relationship('ServerOsType')
    ser_os_version = db.relationship('ServerOsVersion')
    def __init__(self, *args, ** kwargs):
        super(ServersInventory, self).__init__(*args, **kwargs)

    def __repr__(self):
        return "{}".format(self.ser_hostname)




